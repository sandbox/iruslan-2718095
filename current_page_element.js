/**
 * Current page element custom scripts.
 */

(function ($) {

  Drupal.behaviors.currentPathElement = {
    attach: function (context, settings) {
      // Fulfil all hidden fields with value of current path.
      $('.current-page-element', context).once(function () {
        $(this).val(window.location.href);
      });
    }
  };

})(jQuery);
